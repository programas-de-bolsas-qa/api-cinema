# 1- Instalaçao de programas

### 1º passo

fazer a instalação no Nodejs

https://nodejs.org/en/download

### 2º passo

- fazer instalação do chocolatey, para conseguirmos instalar o k6 em seguida.

Entra no site: https://chocolatey.org/install

la encontra-se os comando para instalação.

- Primeiro vamos usar o Chocolatey for Individual:
  ![Alt text](image-7.png)

- Segundo vamos usar um comando para instalação do chocolatey dentro do PowerShell **_no modo administrador_**

copie esse comando:

```
 Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

```

Cole no PowerShell conforme imagem abaixo
![Alt text](image-8.png)

#### Agora sim chocolatey instalado

### 3º passo

- Fazer a instalação do "k6" atraves do choco.
- Vamos usar um comando para instalação do K6 dentro do PowerShell.

Copie esse comando e cole no PowerShell:

`choco install k6`

![Alt text](image-4.png)

Pronto k6 instalado

#### Agora para criação de script e fazer o K6 rodar, indico a intalação do VSCODE e roda os testes.

![Alt text](image-5.png)

# 2- Acessando a API cinema

1- Atravez do link [API-Cinema](https://github.com/juniorschmitz/nestjs-cinema/tree/main) voce irá acessar um projeto feito pelo _**juniorschmitz**_.

- Nesse projeto voce irá fazer o [Download](https://github.com/juniorschmitz/nestjs-cinema/archive/refs/heads/main.zip) do projeto .zip.

* Após baixar o projeto na sua máquina, va ate a pasta onde está os aquivos, abra um terminal nela e escreva os seguintes comando:

```
npm install
```

Para rodar a aplicação, executar o comando:

```
npm run start
```

2- Validando a execução

- Após subir a aplicação, ela estará disponível no endereço http://localhost:3000

3- Visualizando o Swagger

- Para visualizar o Swagger da aplicação, você poderá visitar o endereço http://localhost:3000/api/docs

Agradecimentos:
Raphael - que me ajudou a manter meus codigos limpo
Lucas - me ajudou a encontra um erro na minha Baserest, até entao nao tava conseguindo atualizar os filmes e me ajudou tambem na parte do Map, onde atravez do foreach criar um variavel, para guardar soemnete os id, para que eu conseguisse deletar todos filme listado.
