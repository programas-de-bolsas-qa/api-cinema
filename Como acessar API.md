
# Acessando a API cinema

1- Atravez do link [API-Cinema](https://github.com/juniorschmitz/nestjs-cinema/tree/main ) voce irá acessar um projeto feito pelo _**juniorschmitz**_.

- Nesse projeto voce irá fazer o [Download](https://github.com/juniorschmitz/nestjs-cinema/archive/refs/heads/main.zip) do projeto .zip.

* Após baixar o projeto na sua máquina, va ate a pasta onde está os aquivos, abra um terminal nela e escreva os seguintes comando:

```
npm install
```

Para rodar a aplicação, executar o comando:
```
npm run start
```

2- Validando a execução
- Após subir a aplicação, ela estará disponível no endereço http://localhost:3000

3- Visualizando o Swagger
- Para visualizar o Swagger da aplicação, você poderá visitar o endereço http://localhost:3000/api/docs
