import { sleep } from "k6";
import { SharedArray } from "k6/data";
import {
  BaseChecks,
  BaseRest,
  ENDPOINTS,
  testConfig,
} from "../support/base/baseTest.js";

export const options = testConfig.options.smokeTresholds;

const base_uri = testConfig.environment.hml.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray("Users", function () {
  const jsonData = JSON.parse(open("../data/static/user.json"));
  return jsonData.users;
});

const playload = {
  nome: "Fulano da Silva",
  email: "paulotest93@qa.com.br",
  password: "teste",
  administrador: "true",
};

export function setup() {
  const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, playload);

  baseChecks.checkStatusCode(res, 201);

  return { responseData: res.json() };
}

export default () => {
  let userIndex = __ITER % data.length;
  let user = data[userIndex];

  // console.log(user);

  const urlRes = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, user);
  baseChecks.checkStatusCode(urlRes, 200);

  sleep(1);
};
export function teardown(responseData) {
  const userId = responseData.responseData._id;
  const res = baseRest.delete(ENDPOINTS.USER_ENDPOINT + `/${userId}`);
  console.log(`TEARDOWN deletando o usaurios com ID = ${userId}`);
  baseChecks.checkStatusCode(res, 200);
}
