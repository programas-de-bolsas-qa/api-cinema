import { sleep } from "k6";
import { SharedArray } from "k6/data";
import {
  BaseChecks,
  BaseRest,
  ENDPOINTS,
  testConfig,
} from "../../../support/base/baseTest.js";
import { randomItem } from "https://jslib.k6.io/k6-utils/1.2.0/index.js";

export const options = testConfig.options.smokeTresholds;

const base_uri = testConfig.environment.hml.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray("UsersTickets", function () {
  const jsonData = JSON.parse(open("../../../data/dynamic/tickets.json"));
  return jsonData.UsersTickets;
});

export function setup() {
  // let responseData = [];

  data.forEach((UsersTickets) => {
    console.log(" Reservando um ingresso", UsersTickets);
    const res = baseRest.post(ENDPOINTS.TICKETS_ENDPOINT, UsersTickets);
    baseChecks.checkStatusCode(res, 201);
    // sleep(1);

    // responseData.push(res.json());
  });
}

export default () => {
  const lista = baseRest.get(ENDPOINTS.TICKETS_ENDPOINT);
  baseChecks.checkStatusCode(lista, 200);
  // console.log(lista.body);
  // responseData.push(lista.json());

  // sleep();
};

// export function teardown() {
//   const lista = baseRest.get(ENDPOINTS.TICKETS_ENDPOINT);
//   let listTickets = lista.json().map((tickets) => tickets._id);

//   // console.log(listMovie);

//   listTickets.forEach((id) => {
//     console.log(`teardown deletando o usuario com o ID ${id}`);
//     const res = baseRest.delete(ENDPOINTS.TICKETS_ENDPOINT + `/${id}`);
//     baseChecks.checkStatusCode(res, 200);
//   });
// }
