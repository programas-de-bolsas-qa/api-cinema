import { sleep } from "k6";
import { SharedArray } from "k6/data";
import {
  BaseChecks,
  BaseRest,
  ENDPOINTS,
  testConfig,
} from "../support/base/baseTest.js";
import { randomItem } from "https://jslib.k6.io/k6-utils/1.2.0/index.js";

export const options = testConfig.options.smokeTresholds;

const base_uri = testConfig.environment.hml.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray("UsersMovies", function () {
  const jsonData = JSON.parse(open("../data/dynamic/UsersMovies.json"));
  return jsonData.UsersMovies;
});

export default () => {
  let responseData = [];

  data.forEach((UsersMovies) => {
    console.log(" crinado filme", UsersMovies);
    const res = baseRest.post(ENDPOINTS.MOVIES_ENDPOINT, UsersMovies);
    baseChecks.checkStatusCode(res, 201);
    sleep(1);

    // responseData.push(res.json());
  });
  const lista = baseRest.get(ENDPOINTS.MOVIES_ENDPOINT);
  console.log(lista.body);
  responseData.push(lista.json());
};

// export default () => {
//   const urlRes = baseRest.get(ENDPOINTS.MOVIES_ENDPOINT);
//   baseChecks.checkStatusCode(urlRes, 200);

//   sleep(1);
// };

export function teardown(responseData) {
  const lista = baseRest.get(ENDPOINTS.MOVIES_ENDPOINT);
  let listMovie = lista.json().map((filme) => filme._id);
  const res = baseRest.delete(
    ENDPOINTS.MOVIES_ENDPOINT + `/${listMovie[__VU]._id}`
  );
  // console.log(listMovie);

  // console.log(responseData.responseData);
  // const ids = responseData.responseData.map((item) => item._id);

  listMovie.forEach((id) => {
    console.log(`teardown deletando o usuario com o ID ${id}`);
    const res = baseRest.delete(ENDPOINTS.MOVIES_ENDPOINT + `/${id}`);
    baseChecks.checkStatusCode(res, 200);
  });
}
