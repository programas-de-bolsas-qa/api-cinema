import { sleep } from "k6";

// (inicializa variaveis, define opitions (vus, duartin,tresholds))
let counter = 1;

export function setup() {
  // 2. setup code (executa apenas 1 vez antes da função principal)
  console.log(`SETUP ${counter}`);
}

export default function () {
  // 3. VU code (Ponto de entrada dos VU's, onde realizam os testes/chamadas na API)
  console.log(`FUNÇÃO PRINCIPAL - ${counter} VU=${__VU} INTER=${__ITER}`);
  counter = counter + 1;
  sleep(1);
}

export function teardown() {
  // 4. teardown code ( executa apenas 1 vez apos a execução da funcação principal)
  console.log(`TEARDWON = ${counter}`);
}
