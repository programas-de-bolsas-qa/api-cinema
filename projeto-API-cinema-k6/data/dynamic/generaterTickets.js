const { error } = require("console");
const {
  faker,
} = require("./lib/node_modules/@faker-js/faker/dist/cjs/locale/pt_BR");

const fs = require("fs");

const quantidade = process.argv[2] || 1;

const tickets = [];

for (let i = 0; i < quantidade; i++) {
  const ticket = {
    movieId: faker.internet.userName(),
    userId: faker.internet.email({ provider: "exemplo.qa.com.br" }),
    seatNumber: faker.number.int({ max: 99 }),
    price: faker.number.int({ max: 69 }),
    showtime: "2023-11-27T19:18:46.902Z",
  };

  tickets.push(ticket);
}

const data = {
  UsersTickets: tickets,
};

fs.writeFileSync("tickets.json", JSON.stringify(data, null, 2), (error) => {
  if (error) {
    console.error(error);
  }
});
// fs.writeFileSync(
//   "Userstickets.json",
//   JSON.stringify(data, null, 2),
//   (error) => {
//     if (error) {
//       console.error(error);
//     }
//   }
// );
