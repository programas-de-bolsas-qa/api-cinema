const { error } = require("console");
const {
  faker,
} = require("./lib/node_modules/@faker-js/faker/dist/cjs/locale/pt_BR");

const fs = require("fs");

const quantidade = process.argv[2] || 1;

const movies = [];

for (let i = 0; i < quantidade; i++) {
  const movie = {
    title: faker.internet.userName(),
    description: faker.internet.email({ provider: "exemplo.qa.com.br" }),
    launchdate: faker.internet.userName(),
    showtimes: ["true"],
  };

  movies.push(movie);
}

const data = {
  UsersMovies: movies,
};

fs.writeFileSync("UsersMovies.json", JSON.stringify(data, null, 2), (error) => {
  if (error) {
    console.error(error);
  }
});
