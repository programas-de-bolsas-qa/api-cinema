import http from "k6/http";
import { BaseService } from "./baseService.js";

export class BaseRest extends BaseService {
  constructor(base_uri) {
    super(base_uri);
  }

  //   const res = http.post("http://localhost:3000/usuarios", playload)
  //   base_uri = http://localhost:300
  //   endpoint = /usuarios

  post(endpoint, body, headers = {}, params = {}) {
    let uri = this.base_uri + endpoint;
    let options = this.buildOptions(headers, params);
    return http.post(uri, JSON.stringify(body), options);
  }

  get(endpoint) {
    let uriGet = this.base_uri + endpoint;
    return http.get(uriGet);
  }

  // put(endpoint, body) {
  //   let uriPut = this.base_uri + endpoint;
  //   return http.put(uriPut, JSON.stringify(body));
  // }

  put(endpoint, body, headers = {}, params = {}) {
    let uri = this.base_uri + endpoint;
    let options = this.buildOptions(headers, params);
    return http.put(uri, JSON.stringify(body), options);
  }

  delete(endpoint) {
    let uriDelete = this.base_uri + endpoint;
    return http.del(uriDelete);
  }

  buildOptions(headers = {}, params = {}) {
    return {
      headers: Object.assign({ "Content-type": "application/json" }, headers),
      params: Object.assign({}, params),
    };
  }
}
