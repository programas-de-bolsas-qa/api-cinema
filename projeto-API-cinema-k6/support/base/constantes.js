export const ENDPOINTS = {
  LOGIN_ENDPOINT: "/login",
  USER_ENDPOINT: "/usuarios",
  PRODUCTS_ENDPOINT: "/produtos",
  CARTS_ENDPOINT: "/carrinhos",
  MOVIES_ENDPOINT: "/movies",
  TICKETS_ENDPOINT: "/tickets",
};
