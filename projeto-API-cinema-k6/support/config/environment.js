export const testConfig = {
  environment: {
    hml: {
      url: "http://localhost:3000",
    },
    ec2: {
      url: "http://3.221.160.54:3000",
    },
  },
  // teste de smoke (fumaça)
  options: {
    smokeTresholds: {
      vus: 50,
      duration: "1s",
      thresholds: {
        http_req_duration: ["p(95)<100"],
        http_req_failed: ["rate<0.01"],
      },
    },

    //teste de carga
    loadTresholds: {
      vus: 200,
      duration: "1m",
      thresholds: {
        http_req_duration: ["p(95)<2000"],
        http_req_failed: ["rate<0.01"],
      },
    },
    // teste para stressar a API com amsi carga e mais tempo
    stressTresholds: {
      vus: 400,
      thresholds: {
        http_req_duration: ["p(95)<2000"],
        http_req_failed: ["rate<0.01"],
      },
      stages: [
        { duration: "10s", target: 100 },
        { duration: "10s", target: 150 },
        { duration: "10s", target: 200 },
        { duration: "10s", target: 250 },
        { duration: "10s", target: 300 },
        { duration: "10s", target: 400 },
      ],
    },
  },
};
