#       Planejamento
   

##  Plano de teste e análise da API (Cinema)

### 1. Nome do Projeto

Bugs e melhorias na API Cinema

### 2. Resumo

Testar a funcionalidade e a integridade da API (Cinema), para garantir que ela atenda aos requisitos e funcione conforme esperado.
Neste projeto vamos analisar, e encontrar bugs e melhorias que deverão ser feitas para aumentar o engajamento da API, o desempenho, o tempo de resposta das buscas.

### 3. Pessoas envolvidas

Por ser um projeto de estagio, Paulo Henrique da Silva, estará organizando o projeto, para a entrega de valores e teste ao cliente, colocando as boas praticas e estudos aprendido no método ágil.

### 4. Funcionalidades ou modulos a serem testados

- [ ] Banco de dados e infraestrutura para desenvolvimento disponibilizados;
- [ ] API de cadastro de filmes e reserva de ingresso implementada;
- [ ] Ambiente de testes disponibilizado.

#### Irei dividir em dois modulos

- [ ] Teste rota Filmes
- [ ] teste rota de Ingressos

### 5.Local dos testes / Comunicação

Os testes serão realizados cada um no seu ambiente de trabalho remoto, em um horário escolhido pelos testadores. Haverá uma equipe criada pelo scrum master, no teams, para que haja comunicação entre a equipe. o meio de comunicação por ser feita pelo teams, Telegram ou WhatsApp 

### 6. Recursos necessários

Os recursos necessários serão providos pelos testadores. Para a realização dos testes:

* Ambiente de Teste: Configure um ambiente de teste atraves da API proposta (Cinema)
* Ferramentas: Utilize ferramentas como Postman, teste automatizado, k6 ..
* Documentação: Swagger Tenha a documentação da API (endpoints, parâmetros, respostas) acessível para referência.
* Banco de dados e infraestrutura para desenvolvimento disponibilizados
* API de cadastro de filmes e reserva de ingresso implementada;
* Ambiente de testes disponibilizado.

* instalação do chocolatey
* instalação do k6
* instalação do VSCode
* Execução da API localmente

### 7. Critérios usados

Ultilizar Endpoints para criações, reserva, exclusão.

#### Metrica usada

* 50 usuarios - considerada baixa
* 100 usuarios - considerada media
* 400 usuarios - considerada muito alta

#### Metricas de Desempenho

- thresholds :

  **http_req_failed: ["rate<0.01"] - os erros devem ser menor que 1%**

  **http_req_duration: ["p(95)<1000"] - 95 por cento das requisições devem ser menor a 200 ms**

##### Estrategia de Teste

- Smoke =

      Vus: 50

      duration: 5s

- Carga =

      Vus: 100

      duration: 1m

- stress =

      Vus: 400

      duration: {
        stages: [
        { duration: "10s", target: 100 },
        { duration: "10s", target: 150 },
        { duration: "10s", target: 200 },
        { duration: "10s", target: 250 },
        { duration: "10s", target: 300 },
        { duration: "10s", target: 400 },
      ],
      }

#### Teste de Paginação:

Verificar se a paginação está funcionando corretamente ao solicitar uma quantidade específica de produtos por página. 

### Teste de Rota:


#### Gerenciamento de Filmes

`Criando um Novo Filme`
- [ ] O usuário administrador da API envia uma solicitação POST para o endpoint /movies com os detalhes do filme.
- [ ] O sistema valida os campos obrigatórios e a unicidade do título.
- [ ] Se as validações passarem, o sistema cria o filme e atribui um ID único.
- [ ] O sistema retorna uma resposta de sucesso com o status 201 Created, incluindo o ID do filme.

`Obtendo a Lista de Filmes:`
- [ ] O usuário envia uma solicitação GET para o endpoint /movies.
- [ ] O sistema retorna uma lista de todos os filmes cadastrados com detalhes.


`Obtendo Detalhes de um Filme por ID:`
- [ ] O usuário envia uma solicitação GET para o endpoint /movies/{id}, onde {id} é o ID do filme desejado.
- [ ] O sistema verifica a existência do filme e retorna seus detalhes.
- [ ] Se o filme não existir, o sistema retorna uma resposta de erro com o status 404 Not Found.


`Atualizando os Detalhes de um Filme por ID:`
- [ ] O usuário administrador da API envia uma solicitação PUT para o endpoint /movies/{id}, onde {id} é o ID do filme a ser atualizado.
- [ ] O sistema verifica a existência do filme, permite a atualização de campos específicos e valida os dados.
- [ ] Se todas as validações passarem, o sistema atualiza os detalhes do filme.
- [ ] O sistema retorna uma resposta de sucesso com o status 200 OK e os detalhes atualizados do filme.


`Excluindo um Filme por ID:`
- [ ] O usuário administrador da API envia uma solicitação DELETE para o endpoint /movies/{id}, onde {id} é o ID do filme a ser excluído.
- [ ] O sistema verifica a existência do filme e o remove permanentemente do banco de dados.
- [ ] O sistema retorna uma resposta de sucesso com o status 204 No Content.

#### Requisitos Não Funcionais de Performance:

- [ ] A API deve ser capaz de processar pelo menos 100 solicitações de criação de filmes por segundo.
- [ ] O tempo médio de resposta para a criação de um novo filme não deve exceder 200 milissegundos.
- [ ] A API deve ser capaz de responder a solicitações GET de listagem de filmes em menos de 100 milissegundos.
- [ ] A lista de filmes deve ser paginada, com no máximo 20 filmes por página.
- [ ] A API deve ser capaz de responder a solicitações GET de detalhes de um filme em menos de 50 milissegundos.
- [ ] A API deve ser capaz de processar pelo menos 50 solicitações de atualização de filmes por segundo.
- [ ] O tempo médio de resposta para a atualização dos detalhes de um filme não deve exceder 300 milissegundos.
- [ ] A API deve ser capaz de processar pelo menos 30 solicitações de exclusão de filmes por segundo.
- [ ] O tempo médio de resposta para a exclusão de um filme não deve exceder 400 milissegundos.

#### Reservando Ingressos

- [ ] O usuário envia uma solicitação POST para o endpoint /tickets com os seguintes detalhes do ingresso:
- [ ] ID do Filme (movieId) - Identifica o filme para o qual o ingresso está sendo reservado.
- [ ] ID do Usuário (userId) - Identifica o usuário que está fazendo a reserva.
- [ ] Número do Assento (seatNumber) - O número do assento que o usuário deseja reservar.
- [ ] Preço do Ingresso (price) - O preço do ingresso para o filme.
- [ ] Data de Apresentação (showtime) - A data e hora da apresentação do filme.
- [ ] O sistema valida se todos os campos obrigatórios estão preenchidos corretamente.
- [ ] O sistema verifica se o número do assento está dentro do intervalo de 0 a 99.
- [ ] O sistema verifica se o preço do ingresso está dentro do intervalo de 0 a 60.
- [ ] Se todas as validações passarem, o sistema cria uma reserva de ingresso com os detalhes fornecidos.
- [ ] O sistema atribui um ID único à reserva de ingresso.
- [ ] O sistema retorna uma resposta de sucesso com o status 201 Created, incluindo o ID da reserva de ingresso.

Requisitos Não Funcionais

- [ ] A API deve ser capaz de processar pelo menos 50 solicitações de reserva de ingressos por segundo.
- [ ] O tempo médio de resposta para a reserva de um ingresso não deve exceder 300 milissegundos.

### 8. Riscos

Servidores ou computadores dos QA ficarem sem internet;

A API pode depender de serviços externos, como bancos de dados ou sistemas de autenticação, que podem estar indisponíveis ou inconsistentes.

A API pode não lidar bem com tráfego intenso, resultando em tempos de resposta lentos ou falhas.

A API pode não ser compatível com todos os dispositivos, navegadores ou sistemas operacionais, resultando em erros ou comportamentos inesperados.

A documentação da API pode estar desatualizada, incompleta ou conter informações incorretas, o que pode levar a testes inadequados.

Restrições de tempo podem levar a testes inadequados ou insuficientes, resultando em problemas não detectados.

### 9. Como os resultados do teste serão divulgados

Através dos programas que fornecem os dados testados;
Os cenários de teste levantados, explicando os cenários mais importantes e o valor que eles geram;
Mapa mental criado para fazer um processo de teste;
User stories, através do gitlab, Jira




# MAPA MENTAL

![Mapa mental API cinema](/imagens/Mapa-mental.PNG)